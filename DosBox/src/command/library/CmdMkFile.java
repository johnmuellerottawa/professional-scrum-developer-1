/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package command.library;

import filesystem.File;
import interfaces.IDrive;
import interfaces.IOutputter;

import command.framework.Command;

class CmdMkFile extends Command {

	public CmdMkFile(String cmdName, IDrive drive) {
		super(cmdName, drive);
	}

	@Override
	public void execute(IOutputter outputter) {
		String fileName = this.getParameterAt(0);
		String fileContent;
		if (this.getParameterCount()>1)
			fileContent = this.getParameterAt(1);
		else
			fileContent="";
		
		
		File newFile = new File(fileName, fileContent);
		this.getDrive().getCurrentDirectory().add(newFile);
	}
	
	@Override
	protected boolean checkNumberOfParameters(int numberOfParametersEntered)
    {
        if (numberOfParametersEntered > 0)
        	return true;
         
        return false;
    }
	
	@Override
	public final boolean checkParameters(IOutputter outputter) throws Exception {
		if(!checkNumberOfParameters(this.parameters.size())) {
			outputter.printLine(INCORRECT_SYNTAX);
			return false;
		}
		
		if(!checkParameterValues(outputter)) {
			if (!outputter.hasCharactersPrinted())
				outputter.printLine(DEFAULT_ERROR_MESSAGE_WRONG_PARAMETER);
			return false;
		}
		
		return true;
	}
}
