/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package command.library;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import helpers.TestHelper;
import filesystem.File;

public class CmdTypeTest extends CmdTest {

	@Before
	public void setUp() {
        // Check this file structure in base class: crucial to understand the tests.
        this.createTestFileStructure();
		
		// Add all commands which are necessary to execute this unit test
		// Important: Other commands are not available unless added here.
        this.commandInvoker.addCommand(new CmdType("type", this.drive));
		this.commandInvoker.addCommand(new CmdMkFile("mkfile", this.drive));
		this.commandInvoker.addCommand(new CmdMkDir("mkdir", this.drive));
	}

    @Test
    public void CmdType_PrintGoodFile()
    {
    	// given
        final String newFileName = "testFile";
        final String newFileContent = "HelloWorld";

        // when
        executeCommand("mkfile " + newFileName + " " + newFileContent);

        // when
        executeCommand("type " + newFileName);
        
        // then
       // assertEquals(numbersOfFilesBeforeTest + 1, drive.getCurrentDirectory().getNumberOfContainedFiles());
       
        assertEquals(testOutput.toString(), newFileContent);
    }

    @Test
    public void CmdType_WithoutContent()  {
    	// given
        final String newFileName = "testFile";
        final String newFileContent = "";

        // when
        executeCommand("mkfile " + newFileName + " " + newFileContent);

        // when
        executeCommand("type " + newFileName);
        
        // then
       // assertEquals(numbersOfFilesBeforeTest + 1, drive.getCurrentDirectory().getNumberOfContainedFiles());
       
        assertEquals(testOutput.toString(), newFileContent);
    }

    @Test
    public void CmdType_NoFile()    {
    	// when
        executeCommand("type " + "fakeFile");

        // then
        assertEquals(testOutput.toString().trim(), "The system cannot find the file specified");
    }
    
    @Test
    public void CmdType_printDirectory()    {
    	final String newDir = "testDir";
    	final String dirName = "C:\\testDir";
    	// when
        executeCommand("mkdir " + newDir);
        
    	// when
        executeCommand("type " + dirName);

        // then
        assertEquals(testOutput.toString().trim(), "Access is denied");
    }

   /* @Test
    public void CmdMkFile_NoParameters_ReportsError()
    {
        executeCommand("mkfile");
        assertEquals(numbersOfFilesBeforeTest, drive.getCurrentDirectory().getNumberOfContainedFiles());
        TestHelper.assertContains("syntax of the command is incorrect", testOutput.toString());
    }*/
}

