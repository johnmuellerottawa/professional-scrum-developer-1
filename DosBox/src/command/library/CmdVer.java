package command.library;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import interfaces.IDrive;
import interfaces.IOutputter;
import command.framework.Command;

public class CmdVer extends Command {
	//  minor change 2
	private String configFileName = "/application.cfg";
	private String version;
	private List<String> developers;
	
	public CmdVer(String cmdName, IDrive drive) {
		super(cmdName, drive);
	}
	
	@Override
    protected boolean checkNumberOfParameters(int numberOfParameters) {
        return numberOfParameters == 0 || numberOfParameters == 1;
    }
	
	@Override
    protected boolean checkParameterValues(IOutputter outputter) {
        for(int i=0 ; i<getParameterCount() ; i++)
        {
            if (!getParameterAt(i).equals("/w")) {
                return false;
            }
        }

        return true;
    }
	
	@Override
	public void execute(IOutputter outputter) {
		readInCfg();
		
		if (version != null) {
			outputter.printLine("Version "+version);
		} else {
			outputter.printLine("Version is undefined.");
		}
		
		if (this.getParameterCount() > 0) {
			outputter.printLine("");
			for (String developer:this.getDevelopers()) {
				outputter.printLine(developer);
			}
		}
	}

	private void readInCfg() {
		String line = null;
		developers = new ArrayList<String>();
		
		try {
			InputStream in = this.getClass().getResourceAsStream(configFileName);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

            version = bufferedReader.readLine();
            
            while((line = bufferedReader.readLine()) != null) {
                developers.add(line);
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                configFileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + configFileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
		
	}

	public String getConfigFileName() {
		return configFileName;
	}

	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<String> getDevelopers() {
		return developers;
	}

	public void setDevelopers(List<String> developers) {
		this.developers = developers;
	}

}
