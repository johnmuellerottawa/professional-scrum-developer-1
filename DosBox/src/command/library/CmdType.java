/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package command.library;

import interfaces.IDrive;
import interfaces.IOutputter;

import command.framework.Command;

import filesystem.Directory;
import filesystem.File;
import filesystem.FileSystemItem;

/**Command to print out a file.
 * Example for a command with optional parameters.
 */
class CmdType extends Command {
	
	private static final String SYSTEM_CANNOT_FIND_THE_FILE_SPECIFIED = "The system cannot find the file specified";
    private static final String DESTINATION_IS_DIRECTORY = "Access is denied";
	private File filetoPrint;

	protected CmdType(String name, IDrive drive) {
		super(name, drive);
	}

	@Override
	public boolean checkNumberOfParameters(int numberOfParametersEntered) {
		if (numberOfParametersEntered == 1)
			return true;
		
		return false;
	}
	
	@Override
	public boolean checkParameterValues(IOutputter outputter) {
        if(getParameterCount() > 0) {
            this.filetoPrint = extractAndCheckIfValidFile(this.getParameterAt(0), this.getDrive(), outputter);
            return this.filetoPrint != null;
        }
        else
        {
            this.filetoPrint = null;
            return true;
        }
	}
	@Override
	public boolean checkParameters(IOutputter outputter) throws Exception {
		
		return true;  
	}
	
	@Override
    public void execute(IOutputter outputter) {
		String fileName = this.getParameterAt(0);
		
		
		if (checkParameterValues(outputter) == true){
	
			FileSystemItem fileToPrint = this.getDrive().getItemFromPath(fileName);
			if (fileToPrint != null && ((File)fileToPrint).getFileContent() != null){
				outputter.print(((File)fileToPrint).getFileContent());
			}else{   
				outputter.printLine(SYSTEM_CANNOT_FIND_THE_FILE_SPECIFIED);
			}
		}
		 
    }



    private static File extractAndCheckIfValidFile(String destinationFileName, IDrive drive, IOutputter outputter) {
        FileSystemItem tempDestinationFile = drive.getItemFromPath(destinationFileName);
        if (tempDestinationFile == null)
        {
            outputter.printLine(SYSTEM_CANNOT_FIND_THE_FILE_SPECIFIED);
            return null;
        }
        if (tempDestinationFile.isDirectory())
        {
            outputter.printLine(DESTINATION_IS_DIRECTORY);
            return null;
        }
        return (File)tempDestinationFile;
    }
}
