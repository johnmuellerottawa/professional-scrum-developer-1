/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package command.library;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import filesystem.Directory;
import helpers.TestHelper;
import org.junit.Before;
import org.junit.Test;

public class CmdVerTest extends CmdTest {
	public static final String currentVersion = "Version 1.0.0001";
	public static List<String> developers;

	public static final String badSyntaxString = "The syntax of the command is incorrect.";
	public static final String wrongParameterString = "Wrong parameter entered";

	
	static {
		developers = new ArrayList<String>();
		developers.add("Steve");
		developers.add("Richard");
		developers.add("Giao");
		developers.add("Ellen");
		developers.add("Dana");
		developers.add("Tina");
		developers.add("Darrell");
		developers.add("Curtis");
	}
	
    @Before
    public void setUp()
    {
    	 // Check this file structure in base class: crucial to understand the tests.
        this.createTestFileStructure();
        
        // Add all commands which are necessary to execute this unit test
        // Important: Other commands are not available unless added here.
        commandInvoker.addCommand(new CmdVer("ver", drive));

        
    }

    @Test
    public void cmdVer_getVersionAlone() {
    	executeCommand("ver");
    	assertTrue(testOutput.toString().trim().equals(currentVersion));
    }
    
    @Test
    public void cmdVer_getVersionAndDevelopers() {
    	executeCommand("ver /w");
    	TestHelper.assertContains(currentVersion, testOutput);
    	
    	for (String developer:developers) {
    		TestHelper.assertContains(developer, testOutput);
    	}
    	
    }
    
    @Test
    public void cmdVer_badParameterCount() {
    	executeCommand("ver /w /w");
    	TestHelper.assertContains(badSyntaxString, testOutput);
    	TestHelper.assertContains(wrongParameterString, testOutput);
    }

    @Test
    public void cmdVer_badParameterEntered() {
    	executeCommand("ver /foo");
    	TestHelper.assertContains(wrongParameterString, testOutput);
    }
}
