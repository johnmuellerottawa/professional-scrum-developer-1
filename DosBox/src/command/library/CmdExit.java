/*
 * DOSBox, Scrum.org, Professional Scrum Developer Training
 * Authors: Rainer Grau, Daniel Tobler, Zuehlke Technology Group
 * Copyright (c) 2013 All Right Reserved
 */ 

package command.library;

import interfaces.IDrive;
import interfaces.IOutputter;
import command.framework.Command;
import filesystem.Directory;
import filesystem.FileSystemItem;

/**Command to change current directory.
 * Example for a command with optional parameters.
 */
class CmdExit extends Command {
	
	

	protected CmdExit(String name) {
		super(name, null);
	}

	@Override
	public boolean checkNumberOfParameters(int numberOfParametersEntered) {
		return (numberOfParametersEntered == 0 || numberOfParametersEntered == 1); 
	}
	
	@Override
	public boolean checkParameterValues(IOutputter outputter) {
       /* if(getParameterCount() > 0) {
            this.destinationDirectory = extractAndCheckIfValidDirectory(this.getParameterAt(0), this.getDrive(), outputter);
            return this.destinationDirectory != null;
        }
        else
        {
            this.destinationDirectory = null;
            return true;
        }*/
		return true;
	}
	
	@Override
    public void execute(IOutputter outputter) {
      /*  if (getParameterCount() == 0)
        {
            printCurrentDirectoryPath(this.getDrive().getCurrentDirectory().getPath(), outputter);
        }
        else
        {
            changeCurrentDirectory(this.destinationDirectory, this.getDrive(), outputter);
        }*/
    }
   
}
